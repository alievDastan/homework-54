import React, { Component } from "react";
import classNames from "classnames";

class Cell extends Component {
  constructor() {
    super();
    this.state = { clicked: false};
  }
  handleClick() {
    let { flag } = this.state;
    if (!flag) this.setState({clicked: true});
  };

  render() {
    let { row, column, value } = this.props;
    let { clicked, flag } = this.state;
    let cellsClass = classNames({
      cell: true,
      clicked,
      bomb: value === "☀"
    });
    return (
      <td
        id={`${row}_${column}`}
        className={cellsClass}
        onClick={this.handleClick.bind(this)}
      >
        {clicked && !flag ? value : ""}
        {flag}
      </td>
    );
  }
}

export default Cell;
