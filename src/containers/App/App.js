import React from "react";
import Map from "../Map/Map";
import "./App.css";

const App = () => {
    const widthStyle = {
      width: "800px"
    };
    return (
      <div style={widthStyle}>
        <div className="App">
          <br />
          <Map className="map" />
        </div>
      </div>
    );
}

export default App;
