import React, { Component } from "react";
import Cell from "../Cell/Cell";
import {
  nestedArray,
  populateNestedArray
} from "../../components/Helpers/Helpers";


class Map extends Component {
  constructor() {
    super();
    this.state = {
      theMap: populateNestedArray(nestedArray(6, 6), "☀", 1),
    };
  }

  render() {
    return (
      <div>
        <table>
          <tbody>
            {this.state.theMap.map((item, row) => {
              return (
                <tr key={row} className="mapRow">
                  {item.map((subitem, col) => {
                    return (
                      <Cell
                        key={col}
                        row={row}
                        column={col}
                        value={subitem}
                      />
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <p>Count: {Cell.length}</p>
      </div>
    );
  }
}

export default Map;
