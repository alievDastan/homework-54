export function nestedArray(row: number, col: number) {
  let outerArray: Array<Array<number | string>> = [];
  for (let i = 0; i < row; i++) {
    let innerArray: Array<number | string> = [];
    for (let j = 0; j < col; j++) {
      innerArray.push("");
    }
    outerArray.push(innerArray);
  }
  return outerArray;
}

export function populateNestedArray(
  nestedArray: Array<Array<number | string>>,
  val: string | number,
  count: number
) {
  let rows = nestedArray.length;
  let cols = nestedArray[0].length;
  while (count) {
    let y = floorRand(rows);
    let x = floorRand(cols);
    if (!nestedArray[y][x]) {
      nestedArray[y][x] = val;
      count--;
    }
  }
  return nestedArray;
}

function floorRand(scale: number) {
  return Math.floor(Math.random() * scale);
}
